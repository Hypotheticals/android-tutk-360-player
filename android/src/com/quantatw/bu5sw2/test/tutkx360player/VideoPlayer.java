package com.quantatw.bu5sw2.test.tutkx360player;

import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.g3d.utils.DefaultTextureBinder;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import java.io.IOException;

public class VideoPlayer implements VideoPlayerInterface, SurfaceTexture.OnFrameAvailableListener {
    private String TAG = VideoPlayer.class.getSimpleName();

    // Construct shader program codes.
    private static final String ATTRIBUTE_TEXCOORDINATE = ShaderProgram.TEXCOORD_ATTRIBUTE+"0";
    private static final String VARYING_TEXCOORDINATE = "varTexCoordinate";
    private static final String UNIFORM_TEXTURE = "texture";
    private static final String UNIFORM_CAMERATRANSFORM = "camTransform";
    private String vertexShaderCode =
            "attribute highp vec4 a_position;" +
                    "attribute highp vec2 "+ATTRIBUTE_TEXCOORDINATE+";" +
                    "uniform highp mat4 "+UNIFORM_CAMERATRANSFORM+";" +
                    "varying highp vec2 "+VARYING_TEXCOORDINATE+";" +
                    "void main(){" +
                    " gl_Position = "+UNIFORM_CAMERATRANSFORM+" * a_position;" +
                    " varTexCoordinate = "+ATTRIBUTE_TEXCOORDINATE+";" +
                    "}";
    private String fragmentShaderCode =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "uniform samplerExternalOES "+UNIFORM_TEXTURE+";" +
                    "varying highp vec2 "+VARYING_TEXCOORDINATE+";" +
                    "void main(){" +
                    " gl_FragColor = texture2D("+UNIFORM_TEXTURE+", "+VARYING_TEXCOORDINATE+");" +
                    "}";
    private ShaderProgram program;
    private RenderContext renderContext;

    // For video texture.
    private int[] textures = new int[1];
    private SurfaceTexture videoTexture;
    private int primitiveType = GL20.GL_TRIANGLES;
    private boolean frameAvailable = false;
    private boolean isPlaying = false;

    private VideoDecoderThread videoDecoderThread;
    private Handler handler;
    private Object lock = new Object();

    // For graphic components.
    private Camera camera;
    private Mesh mesh;

    // For VideoPlayerInterface.
    private OnCompletionListener onCompletionListener;

    // For TUTKClient binding.
    private TUTKClient client;

    // The mighty constructor.
    public VideoPlayer(Camera camera, Mesh mesh, TUTKClient client){
        this.camera = camera;
        this.mesh = mesh;
        this.program = new ShaderProgram(vertexShaderCode, fragmentShaderCode);
        this.renderContext = new RenderContext(new DefaultTextureBinder(DefaultTextureBinder.WEIGHTED, 1));

        this.client = client;

        SetupRenderTexture();
        InitVideoDecoder();
    }

    // Setup the actual texture.
    private void SetupRenderTexture(){
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glGenTextures(1, textures, 0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textures[0]);
        videoTexture = new SurfaceTexture(textures[0]);
        videoTexture.setOnFrameAvailableListener(this);
    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        //Log.i(TAG, "New frame available!");
        synchronized (this){
            frameAvailable = true;
        }
    }

    private void InitVideoDecoder() {
        if(handler == null)
            handler = new Handler(Looper.getMainLooper());

        // Try to start video decoding thread from main thread.
        handler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (lock){
                    videoDecoderThread = new VideoDecoderThread(
                            new Surface(videoTexture),
                            client
                    );
                    lock.notify();
                }
            }
        });
    }

    @Override
    public void setOnCompletionListener(OnCompletionListener listener) {

    }

    @Override
    public boolean play(String uri) {
        if(videoDecoderThread == null){
            Log.w(TAG, "Waiting for VideoDecoderThread to ready...");
            synchronized (lock){
                while(videoDecoderThread == null){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        Log.e(TAG, "videoDecoderThread not instantiated yet!");
                        e.printStackTrace();
                        return false;
                    }
                }
            }
        }
        Log.i(TAG, "VideoDecoderThread ready, now start to run decoder...");
        isPlaying = true;
        videoDecoderThread.start();

        return true;
    }

    @Override
    public boolean render() {
        if(!isPlaying)
            return false;

        synchronized (this){
            if(frameAvailable){
                videoTexture.updateTexImage();
                frameAvailable = false;
            }
        }

        // Draw texture onto surface.
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textures[0]);
        renderContext.begin();
        renderContext.setDepthTest(GL20.GL_LEQUAL);
        renderContext.setCullFace(GL20.GL_BACK);
        program.begin();
        program.setUniformMatrix(UNIFORM_CAMERATRANSFORM, camera.combined);
        mesh.render(program, primitiveType);
        renderContext.end();
        program.end();
        return true;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void stop() {
        isPlaying = false;
        if(videoDecoderThread != null && !videoDecoderThread.isInterrupted())
            videoDecoderThread.interrupt();
    }

    @Override
    public int getVideoWidth() {
        return 0;
    }

    @Override
    public int getVideoHeight() {
        return 0;
    }

    @Override
    public boolean isBuffered() {
        return false;
    }

    @Override
    public boolean isPlaying() {
        return isPlaying;
    }

    @Override
    public void dispose() {
        stop();

        videoTexture.detachFromGLContext();
        GLES20.glDeleteTextures(1, textures, 0);
        if(program != null){
            program.dispose();
        }
    }
}
