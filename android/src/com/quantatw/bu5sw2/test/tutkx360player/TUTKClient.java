package com.quantatw.bu5sw2.test.tutkx360player;

import android.util.Log;

import com.tutk.IOTC.AVAPIs;
import com.tutk.IOTC.IOTCAPIs;

import java.nio.ByteBuffer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class TUTKClient {
    private static final String TAG = TUTKClient.class.getSimpleName();

    private String uid;
    private String username;
    private String password;

    private int sessionId = -1;
    private int avIndex = -1;

    private boolean isVideoStarted = false;

    private VideoThread videoThread;
    private BlockingQueue<ByteBuffer> videoFrameQueue;

    public TUTKClient(String uid){
        this(uid, "admin", "888888");
    }

    public TUTKClient(String uid, String username, String password){
        this.uid = uid;
        this.username = username;
        this.password = password;

        this.videoFrameQueue = new ArrayBlockingQueue<ByteBuffer>(30);
    }

    public boolean init(){
        if(IOTCAPIs.IOTC_Initialize2(0) != IOTCAPIs.IOTC_ER_NoERROR){
            Log.e(TAG, "Initialize failed!");
            return false;
        }

        AVAPIs.avInitialize(3);//Init 3 channels for video and 2-way audio.

        // Fetch a session ID.
        sessionId = IOTCAPIs.IOTC_Get_SessionID();
        if(sessionId < 0){
            Log.e(TAG, "Cannot obtain SID for "+uid+": error code "+sessionId);
            return false;
        }

        // Associate UID with SID.
        sessionId = IOTCAPIs.IOTC_Connect_ByUID_Parallel(uid, sessionId);
        if(sessionId < 0){
            Log.e(TAG, "Cannot binf session ID for "+uid+". Error code: "+sessionId);
            return false;
        }

        // Start AV client.
        int[] srvType = new int[1];
        avIndex = AVAPIs.avClientStart(sessionId, username, password, 20000, srvType, 0);
        if(avIndex < 0){
            Log.e(TAG, "Cannot start AV client for "+uid+": error code "+avIndex);
            return false;
        }

        return true;
    }

    public void finalize(){
        if(sessionId < 0 || avIndex < 0)
            return;

        closeVideoStream();

        AVAPIs.avClientStop(avIndex);
        Log.i(TAG, "AV client stopped.");
        IOTCAPIs.IOTC_Session_Close(sessionId);
        Log.i(TAG, "Session closed.");
        AVAPIs.avDeInitialize();
        IOTCAPIs.IOTC_DeInitialize();
        Log.i(TAG, "TUTK client exiting...");
    }

    public BlockingQueue<ByteBuffer> getBlockingQueue(){
        return this.videoFrameQueue;
    }

    public boolean openVideoStream(){
        // Try establish camera video stream.
        if(!isVideoStarted && !SetVideoStream(true)){
            Log.e(TAG, "Failed to open video stream for "+uid);
            return false;
        }

        // Run thread to receive AV data.
        videoThread = new VideoThread();
        videoThread.start();
        return true;
    }

    public void closeVideoStream(){
        if(videoThread != null && !videoThread.isInterrupted())
            videoThread.interrupt();
        SetVideoStream(false);
    }

    private boolean SetVideoStream(boolean start){
        AVAPIs av = new AVAPIs();
        int ret;

        if(start){
            ret = av.avSendIOCtrl(avIndex, AVAPIs.IOTYPE_INNER_SND_DATA_DELAY, new byte[2], 2);
            if(ret < 0){
                Log.e(TAG, "IOTYPE_INNER_SND_DATA_DELAY failed. UID "+uid+": error code "+ret);
                return false;
            }

            int IOTYPE_USER_IPCAM_START = 0x1ff;
            ret = av.avSendIOCtrl(avIndex, IOTYPE_USER_IPCAM_START, new byte[8], 8);
            if(ret < 0){
                Log.e(TAG, "IOTYPE_USER_IPCAM_START failed. UID "+uid+": error code "+ret);
                return false;
            }
        }
        else{
            int IOTYPE_USER_IPCAM_STOP = 0x2ff;
            ret = av.avSendIOCtrl(avIndex, IOTYPE_USER_IPCAM_STOP, new byte[8], 8);
            if(ret < 0){
                Log.e(TAG, "IOTYPE_USER_IPCAM_STOP failed. UID "+uid+": error code "+ret);
                return false;
            }
        }

        isVideoStarted = start;
        return true;
    }

    /**
     * The thread that receives video data from device, then forward the data to video decoder.
     */
    private class VideoThread extends Thread {
        private String TAG = VideoThread.class.getSimpleName();

        static final int VIDEO_BUF_SIZE = 300000;//Buffer size might be not large enough.
        static final int FRAME_INFO_SIZE = 16;

        private boolean isInterrupted;

        public VideoThread(){
            this.isInterrupted = false;
        }

        @Override
        public void run() {
            Log.i(TAG, "Begin video thread. AV index: "+avIndex);

            AVAPIs av = new AVAPIs();
            byte[] frameInfo = new byte[FRAME_INFO_SIZE];
            byte[] videoBuffer = new byte[VIDEO_BUF_SIZE];

            // Loop to receive video data from TUTK cloud.
            isInterrupted = false;
            while(!isInterrupted){
                int[] frameNumber = new int[1];
                int[] actualVidBufSize = new int[1];
                int[] expectedFrmInfoSize = new int[1];
                int[] actualFrmInfoSize = new int[1];

                final int ret = av.avRecvFrameData2(avIndex, videoBuffer, VIDEO_BUF_SIZE, actualVidBufSize,
                        expectedFrmInfoSize, frameInfo, FRAME_INFO_SIZE, actualFrmInfoSize, frameNumber);

                if(ret == AVAPIs.AV_ER_DATA_NOREADY){
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        Log.w(TAG, "VideoThread interrupted while sleeping.");
                        e.printStackTrace();
                    }
                    continue;
                }
                else if(ret == AVAPIs.AV_ER_LOSED_THIS_FRAME){
                    Log.w(TAG, "Frame dropped.");
                    continue;
                }
                else if(ret == AVAPIs.AV_ER_INCOMPLETE_FRAME){
                    Log.w(TAG, "Frame incomplete.");
                    continue;
                }
                else if(ret == AVAPIs.AV_ER_SESSION_CLOSE_BY_REMOTE){
                    Log.e(TAG, "Session closed by remote.");
                    break;
                }
                else if(ret == AVAPIs.AV_ER_REMOTE_TIMEOUT_DISCONNECT){
                    Log.e(TAG, "Disconnected by remote due to timeout.");
                    break;
                }
                else if(ret == AVAPIs.AV_ER_INVALID_SID){
                    Log.e(TAG, "Session ID invalid.");
                    break;
                }
                else if(ret < 0){//Any other error?
                    Log.w(TAG, "'avRecvFrameData2' status unknown: "+ret);
                    continue;
                }

                //FIXME Send video buffer through callback.
                try {
                    Log.d(TAG, "Video frame received... size = "+ret);
                    ByteBuffer temp = ByteBuffer.allocate(ret);
                    temp.put(videoBuffer, 0, ret);
                    videoFrameQueue.put(temp);
                } catch (InterruptedException e) {
                    Log.e(TAG, "Video frame buffer error!");
                    e.printStackTrace();
                }

            }

            Log.i(TAG, "Video frame receiving loop quited.");
        }

        @Override
        public void interrupt() {
            isInterrupted = true;
            super.interrupt();
        }
    }
}
