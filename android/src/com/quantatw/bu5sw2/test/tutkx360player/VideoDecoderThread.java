package com.quantatw.bu5sw2.test.tutkx360player;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;
import android.view.Surface;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;

public class VideoDecoderThread extends Thread {
    private static String TAG = VideoDecoderThread.class.getSimpleName();

    private TUTKClient client;//Use this decoder to play video for this client.
    private BlockingQueue<ByteBuffer> queue;

    private String MIME_TYPE = "video/avc";
    private int VIDEO_WIDTH = 1280;
    private int VIDEO_HEIGHT = 720;

    private MediaCodec decoder;
    private boolean isEOS;

    public VideoDecoderThread(Surface surface, TUTKClient client){
        isEOS = false;
        this.client = client;
        this.queue = this.client.getBlockingQueue();

        try {
            decoder = MediaCodec.createDecoderByType(MIME_TYPE);
        } catch (IOException e) {
            Log.e(TAG, "Cannot create decoder: "+MIME_TYPE);
            e.printStackTrace();
            return;
        }

        MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, VIDEO_WIDTH, VIDEO_HEIGHT);
        byte[] header_sps={0, 0, 0, 1, 39, 100, 0, 40, -84, 26, -48, 10, 0, -73,
                64, 60, 80, -118, -128};
        byte[] header_pps={0, 0, 0, 1, 6, 24, 11, 10, 0, 0, 3, 0, 2, 72, 24, 2,
                72, 24, 2, 0, 4, -96, -128, 0, 0, 0, 1, 6, 5, 22, 25, -93, 57,
                87, 79, -67, 74, 13, -73, -49, -27, -46, -56, -78, -35, 93, 78
                -5, -60, 111, 28, 88, -128, 0, 0, 0, 1, 46, -64, -128, 15, 32};

        format.setByteBuffer("csd-0", ByteBuffer.wrap(header_sps));
        format.setByteBuffer("csd-1", ByteBuffer.wrap(header_pps));
        format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE,  VIDEO_WIDTH * VIDEO_HEIGHT);
        format.setInteger("durationUs", 63446722);

        decoder.configure(format, surface, null, 0);
        decoder.start();
    }

    @Override
    public void run(){
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        boolean isInput = true;
        boolean isFirst = true;
        long startMs = 0;

        while(!isEOS){

            try {
                // Feed input.
                if(isInput){
                    int inIndex = decoder.dequeueInputBuffer(1000);
                    if(inIndex >= 0){
                        ByteBuffer buf;
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            buf = decoder.getInputBuffer(inIndex);
                        else
                            buf = decoder.getInputBuffers()[inIndex];

                        byte[] src = queue.take().array();
                        int size = src.length;

                        //Log.d(TAG, "Decoder received video data at length "+size);
                        buf.clear();
                        buf.put(src, 0, size);
                        buf.clear();

                        if(size > 0){//Normal data.
                            decoder.queueInputBuffer(inIndex, 0, size, 0, 0);
                        }
                        else{//Is EOS.
                            decoder.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                            //isEOS = true;
                            isInput = false;
                        }
                    }
                    else{//inIndex < 0
                        Log.w(TAG, "No input buffer ready: "+inIndex);
                    }
                }


                // Release output.
                int outIndex = decoder.dequeueOutputBuffer(info, 10000);
                if(outIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED){
                    Log.w(TAG, "MediaCodec.INFO_OUTPUT_FORMAT_CHANGED: "+decoder.getOutputFormat());
                }
                else if(outIndex == MediaCodec.INFO_TRY_AGAIN_LATER){
                    Log.w(TAG, "MediaCodec.INFO_TRY_AGAIN_LATER");
                }
                else{//Decoded output available.
                    if(isFirst){
                        startMs = System.currentTimeMillis();
                        isFirst = false;
                    }
                    if(info.presentationTimeUs / 1000 > System.currentTimeMillis() - startMs){
                        sleep(10);
                    }
                    decoder.releaseOutputBuffer(outIndex, true);
                }

                if((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0){
                    Log.i(TAG, "EOS reported.");
                    isEOS = true;
                }

            } catch (InterruptedException e) {
                Log.e(TAG, "VideoDecoderThread interrupted!");
                e.printStackTrace();
                break;
            } catch (Exception e){
                if(e instanceof MediaCodec.CodecException) {
                    Log.w(TAG, "Codec warning: " + e.toString());
                    e.printStackTrace();
                    continue;
                }
                Log.e(TAG, "Other exception: "+e.toString());
                e.printStackTrace();
            }
        }

        decoder.stop();
        decoder.release();
    }

    @Override
    public void interrupt(){
        isEOS = true;//This shall make the while loop quit.
        super.interrupt();
    }
}