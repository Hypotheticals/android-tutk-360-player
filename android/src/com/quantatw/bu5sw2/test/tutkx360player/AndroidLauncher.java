package com.quantatw.bu5sw2.test.tutkx360player;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.quantatw.bu5sw2.test.tutkx360player.Gdx360Display;

public class AndroidLauncher extends AndroidApplication implements SensorEventListener {

    private Gdx360Display display360;

    private SensorManager sensorManager;
    private Sensor sensor;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

        /*
            If gyroscope control is required.
        */
//        config.useAccelerometer = true;
//        config.useGyroscope = true;
//        config.useCompass = false;

        display360 = new Gdx360Display();
        initialize(display360, config);
	}

    @Override
    protected void onResume() {
        super.onResume();
        if(sensorManager == null) {
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        }
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onStop() {
        if(sensorManager != null) {
            sensorManager.unregisterListener(this);
            sensorManager = null;
        }
        super.onStop();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(display360 != null)
            display360.updateRotationVector(new float[]{
                    event.values[0],
                    event.values[1],
                    event.values[2]
            });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {   }
}
