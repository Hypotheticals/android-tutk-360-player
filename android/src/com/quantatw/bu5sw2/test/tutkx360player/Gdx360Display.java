package com.quantatw.bu5sw2.test.tutkx360player;

import android.hardware.SensorManager;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

public class Gdx360Display extends ApplicationAdapter {
	private String TAG = Gdx360Display.class.getSimpleName();

	private TUTKClient client;
	private VideoPlayer player;

	private PerspectiveCamera camera;
	private CameraInputController cameraController;
	private ModelBuilder modelBuilder;
	private Model model;
	private ModelInstance modelInstance;
	private ModelBatch modelBatch;

	@Override
	public void create () {

		client = new TUTKClient("MTYUBVLJ47NCE292111A");
		if(client.init()){
			client.openVideoStream();
		}

		// Init camera.
		camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.position.set(0f, 0f, 0f);
		camera.lookAt(1f, 0f, 0f);
		camera.near = 1f;
		camera.far = 50f;
		camera.update();
		cameraController = new CustomCameraInputController(camera);
		Gdx.input.setInputProcessor(cameraController);

		// Init spherical model.
		modelBuilder = new ModelBuilder();
		model = modelBuilder.createSphere(5f, 5f, 5f, 40, 40, new Material(),
				VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal |
						VertexAttributes.Usage.TextureCoordinates);
        for(Mesh m : model.meshes)
            m.scale(1f, 1f, -1f);
		modelInstance = new ModelInstance(model, 0f, 0f, 0f);
		modelBatch = new ModelBatch();

		// Init video player.
		player = new VideoPlayer(camera, model.meshes.get(0), client);
		player.play(null);

	}

	@Override
	public void render () {

        /*
            If using gyroscope control
         */
//        UpdateCameraByMotionInput();

		cameraController.update();

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		modelBatch.begin(camera);
		modelBatch.render(modelInstance);
		modelBatch.end();

		player.render();
	}

	@Override
	public void resume() {
		player.resume();
	}

	@Override
	public void pause() {
		player.pause();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void dispose() {
		model.dispose();
		modelBatch.dispose();
		if(player != null)
			player.dispose();
		if(client != null){
			client.closeVideoStream();
			client.finalize();
		}

	}

    /*
     *  GYROSCOPE INPUT LOGIC!!
     */
    private float[] rotationVec = new float[3];
    private Matrix4 accMatrix = new Matrix4();
    private Matrix4 gyroMatrix = new Matrix4();
    private Matrix4 initFlip = new Matrix4().setToRotation(Vector3.Z, 90f);
    private final float EPSILON = 0.000000001f;
    private final float FILTER_FACTOR = 0.98f;
    private boolean isInitState = true;
    private float[] fusedMatrix = new float[16];

    private void UpdateCameraByMotionInput(){

        // Get rotation matrix from TYPE_GAME_ROTATION_VECTOR raw data.
        SensorManager.getRotationMatrixFromVector(accMatrix.val, rotationVec);
        accMatrix.mulLeft(initFlip);//To fit OpenGL coordination.

        // Integrate with pure gyro data.
        if(isInitState){
            gyroMatrix.mul(accMatrix);
            isInitState = false;
        }

        float[] deltaQuaternion = new float[4];
        GetRotationQuaternionFromGyro(
                new float[]{Gdx.input.getGyroscopeX(), Gdx.input.getGyroscopeY(), Gdx.input.getGyroscopeZ()},
                deltaQuaternion,
                Gdx.graphics.getDeltaTime()
        );

        Matrix4 deltaMatrix = new Matrix4();
        SensorManager.getRotationMatrixFromVector(deltaMatrix.val, deltaQuaternion);

        gyroMatrix.mulLeft(deltaMatrix);

        CalcFusedRotationMatrix();

        camera.up.set(gyroMatrix.val[Matrix4.M11], gyroMatrix.val[Matrix4.M12], gyroMatrix.val[Matrix4.M10]);
        camera.direction.set(-gyroMatrix.val[Matrix4.M21], -gyroMatrix.val[Matrix4.M22], -gyroMatrix.val[Matrix4.M20]);
        camera.update();
    }

    public void updateRotationVector(float[] in){
        rotationVec[0] = in[0];
        rotationVec[1] = in[1];
        rotationVec[2] = in[2];
    }

    private void GetRotationQuaternionFromGyro(float[] gyroValues,
                                               float[] deltaQuaternion,
                                               float deltaTime){
        float[] normValues = new float[3];
        float omegaMagnitude = (float)Math.sqrt(gyroValues[0] * gyroValues[0] +
                gyroValues[1] * gyroValues[1] +
                gyroValues[2] * gyroValues[2]);
        if(omegaMagnitude > EPSILON){
            normValues[0] = gyroValues[0] / omegaMagnitude;
            normValues[1] = gyroValues[1] / omegaMagnitude;
            normValues[2] = gyroValues[2] / omegaMagnitude;
        }

        float thetaOverTwo = omegaMagnitude * deltaTime / 2;
        float sinThetaOverTwo = MathUtils.sin(thetaOverTwo);
        float cosThetaOverTwo = MathUtils.cos(thetaOverTwo);
        // Swap pitch and roll, then invert new pitch.
        deltaQuaternion[0] = -sinThetaOverTwo * normValues[1];
        deltaQuaternion[1] = sinThetaOverTwo * normValues[0];
        deltaQuaternion[2] = sinThetaOverTwo * normValues[2];
        deltaQuaternion[3] = cosThetaOverTwo;
    }

    private void CalcFusedRotationMatrix(){
        float oneMinusFactor = 1f - FILTER_FACTOR;
        fusedMatrix[0] = FILTER_FACTOR * gyroMatrix.val[0] + oneMinusFactor * accMatrix.val[0];
        fusedMatrix[1] = FILTER_FACTOR * gyroMatrix.val[1] + oneMinusFactor * accMatrix.val[1];
        fusedMatrix[2] = FILTER_FACTOR * gyroMatrix.val[2] + oneMinusFactor * accMatrix.val[2];
        fusedMatrix[4] = FILTER_FACTOR * gyroMatrix.val[4] + oneMinusFactor * accMatrix.val[4];
        fusedMatrix[5] = FILTER_FACTOR * gyroMatrix.val[5] + oneMinusFactor * accMatrix.val[5];
        fusedMatrix[6] = FILTER_FACTOR * gyroMatrix.val[6] + oneMinusFactor * accMatrix.val[6];
        fusedMatrix[8] = FILTER_FACTOR * gyroMatrix.val[8] + oneMinusFactor * accMatrix.val[8];
        fusedMatrix[9] = FILTER_FACTOR * gyroMatrix.val[9] + oneMinusFactor * accMatrix.val[9];
        fusedMatrix[10] = FILTER_FACTOR * gyroMatrix.val[10] + oneMinusFactor * accMatrix.val[10];
        fusedMatrix[3] = fusedMatrix[7] = fusedMatrix[11] = 0f;
        fusedMatrix[12] = fusedMatrix[13] = fusedMatrix[14] = 0f;
        fusedMatrix[15] = 1f;
        System.arraycopy(fusedMatrix, 0, gyroMatrix.val, 0, 16);
    }

    /**
     * Customized CameraInputController. It refines some methods to prevents user from flipping over
     * the camera. Also, it limits the zoom in/out level.
     */
    private class CustomCameraInputController extends CameraInputController {
        private float zoomAmount = 0f;
        private float ZOOM_IN_LIMIT = 1f;
        private float ZOOM_OUT_LIMIT = -5f;

        private boolean isInMotionMode = false;
        private boolean isProcessedAsDrag = false;

        public CustomCameraInputController(Camera camera) {
            super(camera);
        }

        public void setInMotionMode(boolean inMotionMode){
            this.isInMotionMode = inMotionMode;
            if(!isInMotionMode)//If switched back to touch mode.
                resetZoom();
        }

//        @Override
//        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
//            if(isProcessedAsDrag)
//                isProcessedAsDrag = false;
//            else {
//                if (Gdx360Display.this.onClickListener != null)
//                    Gdx360Display.this.onClickListener.onClick();
//            }
//            return super.touchUp(screenX, screenY, pointer, button);
//        }

        @Override
        protected boolean process(float deltaX, float deltaY, int button) {
            if(isInMotionMode)
                return false;

            if(button == rotateButton){//button = 0
                Vector3 nv = new Vector3();
                nv.set(this.camera.direction).crs(Vector3.Y);

                isProcessedAsDrag = true;

                if(nv.dst2(0f, 0f, 0f) < 0.0225 &&                         // Point to either poles.
                        ((this.camera.direction.y > 0 && deltaY < 0) ||
                                (this.camera.direction.y < 0 && deltaY > 0))) {
                    return true;
                }

                Vector3 temp = new Vector3();
                temp.set(this.camera.direction).crs(camera.up);
                this.camera.rotateAround(target, temp.nor(), deltaY * -rotateAngle);
                this.camera.rotateAround(target, Vector3.Y, deltaX * rotateAngle);
            }

            if(autoUpdate)
                camera.update();
            return true;
        }

        @Override
        public boolean zoom(float amount) {
            if(isInMotionMode)
                return false;

            float newZoomAmount = zoomAmount + amount;
            if(newZoomAmount < ZOOM_IN_LIMIT && newZoomAmount > ZOOM_OUT_LIMIT){
                zoomAmount = newZoomAmount;
                return super.zoom(amount);
            }
            return true;
        }

        public void resetZoom(){
            this.zoomAmount = 0f;
        }

    }
}
