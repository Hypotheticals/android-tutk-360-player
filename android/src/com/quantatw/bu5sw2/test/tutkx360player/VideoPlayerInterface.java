package com.quantatw.bu5sw2.test.tutkx360player;

import com.badlogic.gdx.utils.Disposable;

public interface VideoPlayerInterface extends Disposable {

    interface OnCompletionListener {
        void onCompletion(String uri);
    }

    void setOnCompletionListener(OnCompletionListener listener);

    boolean play(String uri);
    boolean render();
    void resize(int width, int height);
    void pause();
    void resume();
    void stop();

    int getVideoWidth();
    int getVideoHeight();

    boolean isBuffered();
    boolean isPlaying();

    @Override
    void dispose();
}
